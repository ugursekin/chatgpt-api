<form method="POST" action="{{ route('gpt.store') }}">
    @csrf

    <div>
        <label for="name">Prompt</label>
        <input id="name" type="text" name="prompt" value="" required autofocus>
        @error('name')
            <span role="alert">{{ $message }}</span>
        @enderror
    </div>

    <div>
        <label for="email">Keys</label>
        <input id="email" type="text" name="keys" value="{{ old('email') }}" required>
        @error('email')
            <span role="alert">{{ $message }}</span>
        @enderror
    </div>

    <div>
        <label for="password">Count</label>
        <input type="integer" name="count" required>
        @error('password')
            <span role="alert">{{ $message }}</span>
        @enderror
    </div>

    <input type="submit" value="Gonder">


</form>