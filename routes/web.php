<?php

use App\Http\Controllers\ChatGPTAPI;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/chat-gpt-api', [ChatGPTAPI::class, 'index'])->name('gpt.index'); // silinecek
Route::post('/chat-gpt-api', [ChatGPTAPI::class, 'store'])->name('gpt.store'); // silinecek
